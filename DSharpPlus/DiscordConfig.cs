﻿namespace DSharpPlus
{
    public class DiscordConfig
    {
        public Branch DiscordBranch { get; set; } = Branch.Stable;
        public string Token { get; set; } = "MzIzNzA2NTM0OTI2MjIxMzEy.DB_XAw.Bs1EpOGtG3ifrWpCeTFGCUXih4E";
        public TokenType TokenType { get; set; } = TokenType.Bot;
        public LogLevel LogLevel { get; set; } = LogLevel.Info;
        public bool UseInternalLogHandler { get; set; } = false;
        public int LargeThreshold { get; set; } = 50;
        public bool AutoReconnect { get; set; } = false;
    }
}
