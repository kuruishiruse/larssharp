﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.IO;
using DSharpPlus.Commands;

namespace DSharpPlus.Bot {

    //TODO: Memoize DB responses, only update on update queries
    internal sealed class LarsBotData {

        private SQLiteConnection db;
        private Random rng = new Random();

        private List<string> fortunes;
        private List<string> comforts;
        private List<string> outfits;
        private List<string> tsunderes;
        private List<string> yanderes;
        private List<string> childcomforts;
        private List<string> childoutfits;
        private List<string> childrcomforts;

        private readonly Dictionary<Response, string> ResponseToStringMap = new Dictionary<Response, string>() {
            {Response.Comfort, "comforts" },
            {Response.RComfort, "rcomforts" },
            {Response.Outfit, "outfits" },
            {Response.ChildComfort, "childcomforts" },
            {Response.ChildRComfort, "childrcomforts" },
            {Response.ChildOutfit, "childoutfits" },
            {Response.Fortune, "fortunes" }
        };

        private readonly Dictionary<string, Gender> StringToGenderMap = new Dictionary<string, Gender>() {
            {"Female", Gender.Female},
            {"Male", Gender.Male},
            {"Unspecified", Gender.Unspecified}
        };

        public LarsBotData() { init(); }

        #region initializers
        //TODO: hash custom strings and use those and discordID as primary keys
        private void init() {

            //Create DB if not exists, otherwise open connection
            if (!System.IO.File.Exists("LarsbotDB.sqlite")) {

                SQLiteConnection.CreateFile("LarsbotDB.sqlite");
                using (db = new SQLiteConnection("Data Source=LarsbotDB.sqlite")) {

                    List<string> queries = new List<string>();
                    db.Open();

                    queries.Add("create table users (name varchar(32), discordID int primary key, gender varchar(12))");
                    queries.Add("create table spouses (name varchar(32), sother int references users(discordID) primary key, gender varchar(12))");
                    queries.Add("create table children (name varchar(32), parent int references users(discordID), gender varchar(12))");
                    queries.Add("create table comforts (text varchar(256), author int references users(discordID))");
                    queries.Add("create table rcomforts (text varchar(256), author int references users(discordID))");
                    queries.Add("create table fortunes (text varchar(256), author int references users(discordID))");
                    queries.Add("create table outfits (text varchar(256), author int references users(discordID))");
                    queries.Add("create table tsunderes (text varchar(256), author int references users(discordID))");
                    queries.Add("create table yanderes (text varchar(256), author int references users(discordID))");
                    queries.Add("create table childcomforts (text varchar(256), author int references users(discordID))");
                    queries.Add("create table childoutfits (text varchar(256), author int references users(discordID))");
                    queries.Add("create table childrcomforts (text varchar(256), author int references users(discordID))");

                    foreach (string sql in queries)
                        SQLExecuteNonQuery(sql);

                    db.Close();

                }

            }

            db = new SQLiteConnection("Data Source=LarsbotDB.sqlite");
            db.Open();

            LoadStrings();

        }

        public void LoadStrings() {

            try {

                var fortuneFile = File.ReadAllLines("strings/fortunes.txt");
                fortunes = new List<string>(fortuneFile);

                var comfortFile = File.ReadAllLines("strings/comforts.txt");
                comforts = new List<string>(comfortFile);

                var outfitFile = File.ReadAllLines("strings/outfits.txt");
                outfits = new List<string>(outfitFile);

                var tsundereFile = File.ReadAllLines("strings/tsundere.txt");
                tsunderes = new List<string>(tsundereFile);

                var yandereFile = File.ReadAllLines("strings/yandere.txt");
                yanderes = new List<string>(yandereFile);

                var childComfortFile = File.ReadAllLines("strings/childcomforts.txt");
                childcomforts = new List<string>(childComfortFile);

                var childOutfitFile = File.ReadAllLines("strings/childoutfits.txt");
                childoutfits = new List<string>(childComfortFile);

                var childRComfortFile = File.ReadAllLines("strings/childrcomforts.txt");
                childrcomforts = new List<string>(childComfortFile);

            } catch (FileNotFoundException fnf) {
                Console.WriteLine(fnf.Message);
            } 

        }
        #endregion

        #region command functions
        internal void CloseDB() => db.Close();

        internal async Task<string> SpouseReg(DiscordUser author, string spouse, Gender g) {

            RegisterUser(author.Username, author.ID);
            SQLExecuteNonQuery(String.Format($"insert or replace into spouses values ('{spouse}', {author.ID},'{g}')"));
            return String.Format("{0} registered {1} as their {2}.", author.Username, spouse, g == Gender.Female ? "waifu" : "husbando");

        }

        internal async Task<string> ChildReg(DiscordUser author, string child, Gender g) {

            RegisterUser(author.Username, author.ID);
            SQLExecuteNonQuery(String.Format($"insert or replace into children values ('{child}', {author.ID}, '{g}')"));
            return String.Format("{0} registered {1} as their {2}.", author.Username, child, g == Gender.Female ? "daughter" : "son");

        }

        //TODO: make this a map too
        internal string GetResponse(DiscordUser author, Response r, Reverse reverse) {

            List<string> stringList;
            Reverse doReverse = Reverse.No;
            string response = "";

            switch(r) {

                case Response.Comfort:
                    doReverse = reverse;
                    stringList = comforts;
                    break;

                case Response.Fortune:
                    stringList = fortunes;
                    break;

                case Response.Outfit:
                    stringList = outfits;
                    break;

                case Response.ChildComfort:
                    stringList = childcomforts;
                    break;

                case Response.ChildRComfort:
                    stringList = childrcomforts;
                    break;

                case Response.ChildOutfit:
                    stringList = childoutfits;
                    break;

                case Response.Tsundere:
                    stringList = tsunderes;
                    break;

                case Response.Yandere:
                    stringList = yanderes;
                    break;

                default:
                    stringList = new List<string>();
                    break;

            }

            response = getRandomString(author, stringList, r, doReverse);
            return response;

        }

        internal async Task<string> GetSpouseName(string s) {

            return getSpouseNameFromString(s);

        }

        internal async Task<string> GetSpouseName(ulong iD) {

            return getSpouseNameFromID(iD);

        }

        internal async Task<Gender> GetSpouseGender(string s) {

            ulong ID = getIDFromString(s);
            return getSpouseGenderFromID(ID);

        }

        internal async Task<Gender> GetSpouseGender(ulong ID) {

            return getSpouseGenderFromID(ID);

        }

        internal async Task<Gender> GetGender(string s) {

            ulong id = getIDFromString(s);
            return getGenderFromID(id);

        }

        internal async Task<Gender> GetGender(DiscordUser author) {

            return getGenderFromID(author.ID);

        }

        internal string truncateDB() {

            SQLExecuteNonQuery("delete from users; delete from spouses");
            return "Database truncated.";

        }

        internal async Task<string> GenderSet(DiscordUser author, Gender g) {

            SQLExecuteNonQuery(String.Format($"update users set gender = '{g}' where discordID = {author.ID}"));
            return String.Format($"Set user {author.Username}'s gender to {g}.");

        }

        internal async Task AddResponse(ulong ID, string s, Response r) {

            string table = ResponseToStringMap[r];
            SQLExecuteNonQuery(String.Format($"insert into {table} values ('{s}', {ID})"));

        }

        private void RegisterUser(string userName, ulong ID) {

            SQLExecuteNonQuery(String.Format($"insert or ignore into users values ('{userName}', {ID}, '{Gender.Unspecified}')"));

        }
        #endregion

        #region replacements
        private string replaceNames(DiscordUser author, string s, Reverse reverse) {

            var sotherName = getSpouseNameFromID(author.ID);
            List<string> childList = getChildrenFromID(author.ID);
            string childName = "";

            if(childList.Count > 0)
                childName = childList[rng.Next(childList.Count)];

            Dictionary<string, string> replacements = new Dictionary<string, string>() {
                { "<anon>",  reverse == Reverse.Yes ? sotherName : author.Username },
                { "<waifu>", reverse == Reverse.Yes ? author.Username : sotherName },
                { "<child>", childName }
            };

            var parsedString = replacements.Aggregate(s, (current, value) =>
                                    current.Replace(value.Key, value.Value));

            return parsedString;

        }

        private string replacePronouns(DiscordUser author, string s, Reverse reverse) {

            var sotherGender = getSpouseGenderFromID(author.ID);
            var userGender = getGenderFromID(author.ID);

            bool isUserMale     = userGender    == Gender.Male;
            bool isUserFemale   = userGender    == Gender.Female;
            bool isSpouseMale   = sotherGender  == Gender.Male;
            bool isSpouseFemale = sotherGender  == Gender.Female;

            //TODO: fix for unspecified
            if(reverse == Reverse.Yes && userGender != Gender.Unspecified) {
                isUserMale = !isUserMale;
                isUserFemale = !isUserFemale;
            }

            if(reverse == Reverse.Yes && sotherGender != Gender.Unspecified) {
                isSpouseMale = !isSpouseMale;
                isSpouseFemale = !isSpouseFemale;
            }

            Dictionary<string, string> replacements = new Dictionary<string, string>() {
                { "<aacc>", isUserMale      ? "him"     : isUserFemale      ? "her"     : "they"        },
                { "<agen>", isUserMale      ? "his"     : isUserFemale      ? "her"     : "their"       },
                { "<anom>", isUserMale      ? "he"      : isUserFemale      ? "she"     : "their"       },
                { "<apos>", isUserMale      ? "his"     : isUserFemale      ? "hers"    : "theirs"      },
                { "<aref>", isUserMale      ? "himself" : isUserFemale      ? "herself" : "themself"    },
                { "<wacc>", isSpouseMale    ? "him"     : isSpouseFemale    ? "her"     : "they"        },
                { "<wgen>", isSpouseMale    ? "his"     : isSpouseFemale    ? "her"     : "their"       },
                { "<wnom>", isSpouseMale    ? "he"      : isSpouseFemale    ? "she"     : "their"       },
                { "<wpos>", isSpouseMale    ? "his"     : isSpouseFemale    ? "hers"    : "theirs"      },
                { "<wref>", isSpouseMale    ? "himself" : isSpouseFemale    ? "herself" : "themself"    }
            };

            var parsedString = replacements.Aggregate(s, (current, value) =>
                                    current.Replace(value.Key, value.Value));

            return parsedString;

        }
        #endregion

        #region getters from DB
        //TODO: Fix commands throwing exceptions when performed without S/O
        //Find a default value method for SQLite, or check in each method, or input default value into DB
        private ulong getIDFromString(string s) {

            return SQLReadUlong(String.Format($"select discordID from users where name = '{s}'"));

        }

        private string getSpouseNameFromID(ulong ID) {

            return SQLReadString(String.Format($"select name from spouses where sother = {ID}"));

        }

        private string getSpouseNameFromString(string s) {

            ulong ID = getIDFromString(s);
            return getSpouseNameFromID(ID);

        }

        private Gender getSpouseGenderFromID(ulong ID) {

            string gender = SQLReadString(String.Format($"select gender from spouses where sother = {ID}"));
            return StringToGenderMap[gender];

        }

        private Gender getGenderFromID(ulong ID) {

            string gender = SQLReadString(String.Format($"select gender from users where discordID = {ID}"));
            return StringToGenderMap[gender];

        }

        private List<string> getChildrenFromID(ulong ID) {

            List<string> children = SQLReadList(String.Format($"select name, gender from children where parent ={ID}"));
            return children;

        }

        private string getRandomString(DiscordUser author, List<string> list, Response r, Reverse reverse) {

            string table = ResponseToStringMap[r];
            List<string> customStrings = SQLReadList(String.Format($"select text from {table} where author = {author.ID}"));
            IEnumerable<string> combinedList = list.Concat(customStrings);

            string s = combinedList.ElementAt(rng.Next(combinedList.Count()));
            s = replaceNames(author, s, reverse);
            s = replacePronouns(author, s, reverse);
            return s;

        }
        #endregion

        #region SQL queries
        private void SQLExecuteNonQuery(string sql) {

            Console.WriteLine(sql);
            SQLiteCommand cmd = new SQLiteCommand(sql, db);
            cmd.ExecuteNonQuery();

        }

        private string SQLReadString(string sql) {

            SQLiteCommand cmd = new SQLiteCommand(sql, db);
            SQLiteDataReader r = cmd.ExecuteReader();

            if (r.HasRows) {

                r.Read();
                return r.GetString(0);

            }

            else
                return "???";
            

        }

        private ulong SQLReadUlong(string sql) {

            SQLiteCommand cmd = new SQLiteCommand(sql, db);
            SQLiteDataReader r = cmd.ExecuteReader();

            if (r.HasRows) {

                r.Read();
                return (ulong)r.GetInt64(0);

            }

            else
                return 0;

        }

        private List<string> SQLReadList(string sql) {

            List<string> strings = new List<string>();

            SQLiteCommand cmd = new SQLiteCommand(sql, db);
            SQLiteDataReader r = cmd.ExecuteReader();

            while(r.Read())
                strings.Add(r.GetString(0));

            return strings;

        }
        #endregion

    }

    #region enums
    public enum Gender {

        Male,
        Female,
        Unspecified

    }

    public enum Reverse {

        Yes,
        No

    }

    public enum Response {

        Comfort,
        RComfort,
        Fortune,
        Outfit,
        Tsundere,
        Yandere,
        ChildComfort,
        ChildOutfit,
        ChildRComfort

    }
    #endregion

}
