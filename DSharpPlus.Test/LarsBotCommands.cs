﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DSharpPlus.Commands;
using System.Reflection;
using System.Text;

namespace DSharpPlus.Bot
{
    public sealed class LarsBotCommands
    {
        private LarsBotData data = new LarsBotData();

        #region Default commands
        /**
        public async Task Test(CommandEventArgs e) =>
            await e.Channel.SendMessage("u w0t m8");

        public async Task Testerino(CommandEventArgs e)
        {
            await e.Discord.SendMessage(e.Channel, "ill bash ur head in i sweak on me fkin mum");
            await e.Discord.SendMessage(e.Message.ChannelID, $@"```
Servername: {e.Guild.Name}
Serverowner: {e.Guild.OwnerID}
```");
        }

        public async Task Kill(CommandEventArgs e)
        {
            await e.Channel.SendMessage("kthxbai 👋");
            data.CloseDB();
            e.Discord.Dispose();
            await Task.Delay(-1);
        }

        public async Task Restart(CommandEventArgs e) =>
            await e.Discord.Reconnect();

        public async Task PurgeChannel(CommandEventArgs e)
        {
            var ids = (await e.Channel.GetMessages(before: e.Message.ID, limit: 50))
                .Select(y => y.ID)
                .ToList();
            await e.Channel.BulkDeleteMessages(ids);
            await e.Message.Respond($"Removed `{ids.Count}` messages");
        }

        public async Task Presence(CommandEventArgs e) =>
            await e.Message.Respond(e.Author.Username);

        public async Task Guild(CommandEventArgs e)
        {
            var roles = e.Guild.Roles.Select(xr => xr.Mention);
            var overs = e.Channel.PermissionOverwrites.Select(xo => string.Concat("Principal: ", xo.ID, " (", xo.Type, "), Allow: ", (ulong)xo.Allow, "; Deny: ", (ulong)xo.Deny));

            var embed = new DiscordEmbed
            {
                Title = "Guild info",
                Description = "ye boiii!",
                Type = "rich",
                Color = 0x007FFF,
                Fields = new List<DiscordEmbedField>
                {
                    new DiscordEmbedField
                    {
                        Inline = false,
                        Name = "Roles",
                        Value = string.Join("\n", roles)
                    },
                    new DiscordEmbedField
                    {
                        Inline = false,
                        Name = string.Concat("Overrides for ", e.Channel.Mention),
                        Value = string.Join("\n", overs)
                    }
                }
            };

            await e.Message.Respond("", embed: embed);
        }

        public async Task Embed(CommandEventArgs e)
        {
            List<DiscordEmbedField> fields = new List<DiscordEmbedField>
            {
                new DiscordEmbedField()
                {
                    Name = "This is a field",
                    Value = "it works :p",
                    Inline = false
                },
                new DiscordEmbedField()
                {
                    Name = "Multiple fields",
                    Value = "cool",
                    Inline = false
                }
            };

            DiscordEmbed embed = new DiscordEmbed
            {
                Title = "Testing embed",
                Description = "It works!",
                Type = "rich",
                Url = "https://github.com/NaamloosDT/DSharpPlus",
                Color = 8257469,
                Fields = fields,
                Author = new DiscordEmbedAuthor()
                {
                    Name = "DSharpPlus team",
                    IconUrl = "https://raw.githubusercontent.com/NaamloosDT/DSharpPlus/master/logo_smaller.png",
                    Url = "https://github.com/NaamloosDT/DSharpPlus"
                },
                Footer = new DiscordEmbedFooter()
                {
                    Text = "I am a footer"
                },
                Image = new DiscordEmbedImage()
                {
                    Url = "https://raw.githubusercontent.com/NaamloosDT/DSharpPlus/master/logo_smaller.png",
                    Height = 50,
                    Width = 50,
                },
                Thumbnail = new DiscordEmbedThumbnail()
                {
                    Url = "https://raw.githubusercontent.com/NaamloosDT/DSharpPlus/master/logo_smaller.png",
                    Height = 10,
                    Width = 10
                }
            };
            await e.Message.Respond("testing embed:", embed: embed);
        }

        public async Task AppInfo(CommandEventArgs e)
        {
            var app = await e.Discord.GetCurrentApp();
            var usrn = app.Owner.Username
                .Replace(@"\", @"\\")
                .Replace(@"*", @"\*")
                .Replace(@"_", @"\_")
                .Replace(@"~", @"\~")
                .Replace(@"`", @"\`");

            var embed = new DiscordEmbed
            {
                Title = "Application info",
                Color = 0x007FFF,
                Fields = new List<DiscordEmbedField>()
            };
            embed.Fields.Add(new DiscordEmbedField { Inline = true, Name = "Name", Value = app.Name });
            embed.Fields.Add(new DiscordEmbedField { Inline = true, Name = "Description", Value = string.Concat("```\n", app.Description, "\n```") });
            embed.Fields.Add(new DiscordEmbedField { Inline = true, Name = "ID", Value = app.ID.ToString() });
            embed.Fields.Add(new DiscordEmbedField { Inline = true, Name = "Created", Value = app.CreationDate.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss") });
            embed.Fields.Add(new DiscordEmbedField { Inline = true, Name = "Owner", Value = $"{usrn}#{app.Owner.Discriminator.ToString("0000")} ({app.Owner.ID})" });

            await e.Message.Respond("", embed: embed);
        }

        public async Task Mention(CommandEventArgs e) =>
            await e.Message.Respond($"User {e.Author.Mention} invoked this from {e.Channel.Mention}");

        public async Task ModifyMe(CommandEventArgs e) =>
            await e.Discord.ModifyMember(e.Guild.ID, e.Author.ID, "Tests D#+ instead of going outside");
        **/
        #endregion

        #region kurui's Commands - visible
        public async Task Help(CommandEventArgs e) {

            var methodInfos = typeof(LarsBotCommands)
                .GetMethods(BindingFlags.Public | BindingFlags.Instance)
                .Where(m => m.DeclaringType != typeof(object));

            StringBuilder sb = new StringBuilder("");

            foreach(MethodInfo m in methodInfos)
                sb.AppendFormat("!{0}{1}", m.Name.ToLower(), Environment.NewLine);

            List<DiscordEmbedField> fields = new List<DiscordEmbedField> {

                new DiscordEmbedField() {

                    Name = "Available Commands",
                    Value = sb.ToString(),
                    Inline = false

                },

            };

            DiscordEmbed embed = new DiscordEmbed {

                Type = "rich",
                Color = 8257469,
                Fields = fields,

            };

            await e.Discord.SendMessage(await e.Discord.CreateDM(e.Author.ID), "", embed: embed);

        }

        public async Task WaifuReg(CommandEventArgs e)
            => await SpouseReg(e, Gender.Female);

        public async Task HusbandoReg(CommandEventArgs e)
            => await SpouseReg(e, Gender.Male);

        public async Task DaughteruReg(CommandEventArgs e)
            => await ChildReg(e, Gender.Female);

        public async Task SonfuReg(CommandEventArgs e)
            => await ChildReg(e, Gender.Male);

        public async Task Comfort(CommandEventArgs e) {

            string s = data.GetResponse(e.Author, Response.Comfort, Reverse.No);
            await e.Message.Respond(s);

        }

        public async Task RComfort(CommandEventArgs e) {

            string s = data.GetResponse(e.Author, Response.Comfort, Reverse.Yes);
            await e.Message.Respond(s);

        }

        public async Task DComfort(CommandEventArgs e) {

            string s = data.GetResponse(e.Author, Response.ChildComfort, Reverse.No);
            await e.Message.Respond(s);

        }

        public async Task DRComfort(CommandEventArgs e) {

            string s = data.GetResponse(e.Author, Response.ChildRComfort, Reverse.No);
            await e.Message.Respond(s);

        }

        public async Task Tsundere(CommandEventArgs e) {

            string s = data.GetResponse(e.Author, Response.Tsundere, Reverse.No);
            await e.Message.Respond(s);

        }

        public async Task Yandere(CommandEventArgs e) {

            string s = data.GetResponse(e.Author, Response.Yandere, Reverse.No);
            await e.Message.Respond(s);

        }

        public async Task Outfit(CommandEventArgs e) {

            string s = data.GetResponse(e.Author, Response.Outfit, Reverse.No);
            await e.Message.Respond(s);

        }

        public async Task Hug(CommandEventArgs e) {

            string s = await data.GetSpouseName(e.Author.ID);
            await e.Message.Respond(String.Format($"{e.Author.Username} hugs {s}"));

        }

        public async Task Waifu(CommandEventArgs e)         
            => await Spouse(e);

        public async Task Husbando(CommandEventArgs e)      
            => await Spouse(e);

        public async Task AddComfort(CommandEventArgs e)    
            => await data.AddResponse(e.Author.ID, splitCommand(e.Message.Content, 2)[1], Response.Comfort);

        public async Task AddRComfort(CommandEventArgs e)   
            => await data.AddResponse(e.Author.ID, splitCommand(e.Message.Content, 2)[1], Response.RComfort);

        public async Task AddOutfit(CommandEventArgs e)     
            => await data.AddResponse(e.Author.ID, splitCommand(e.Message.Content, 2)[1], Response.Outfit);

        public async Task AddChildComfort(CommandEventArgs e)
            => await data.AddResponse(e.Author.ID, splitCommand(e.Message.Content, 2)[1], Response.ChildComfort);

        public async Task AddChildRComfort(CommandEventArgs e)
            => await data.AddResponse(e.Author.ID, splitCommand(e.Message.Content, 2)[1], Response.ChildRComfort);

        public async Task AddChildOutfit(CommandEventArgs e)
            => await data.AddResponse(e.Author.ID, splitCommand(e.Message.Content, 2)[1], Response.ChildOutfit);

        public async Task gender(CommandEventArgs e) {

            string[] split = splitCommand(e.Message.Content, 2);
            string sother = "";
            Gender g = Gender.Unspecified;

            if (split.Length == 1) {
                sother = e.Author.Username;
                g = await data.GetGender(e.Author);
            }
            else {
                sother = split[split.Length - 1];
                g = await data.GetGender(split[split.Length - 1]);
            }

            await e.Message.Respond(String.Format($"{sother} is {g}."));

        }

        public async Task GenderSet(CommandEventArgs e) {

            string[] split = splitCommand(e.Message.Content, 2);

            Gender g;
            if (split[split.Length - 1].Equals("male", StringComparison.InvariantCultureIgnoreCase))
                g = Gender.Male;
            else if (split[split.Length - 1].Equals("female", StringComparison.InvariantCultureIgnoreCase))
                g = Gender.Female;
            else
                g = Gender.Unspecified;

            await e.Message.Respond(await data.GenderSet(e.Author, g));

        }

        public async Task Fortune(CommandEventArgs e) {

            string s = data.GetResponse(e.Author, Response.Fortune, Reverse.No);
            await e.Message.Respond(s);

        }

        public async Task truncateDB(CommandEventArgs e) {

            string s = data.truncateDB();
            await e.Message.Respond(s);

        }
        #endregion

        #region kurui's commands - internal
        private async Task SpouseReg(CommandEventArgs e, Gender g) {

            string[] split = splitCommand(e.Message.Content, 2);

            if (split.Length > 1) {
                string s = await data.SpouseReg(e.Author, split[split.Length - 1], g);
                await e.Message.Respond(s);
            }

            else
                await e.Message.Respond($"You need to specify your {(g == Gender.Female ? "waifu" : "husbando")}.");

        }

        private async Task ChildReg(CommandEventArgs e, Gender g) {

            string[] split = splitCommand(e.Message.Content, 2);

            if (split.Length > 1) {
                string s = await data.ChildReg(e.Author, split[split.Length - 1], g);
                await e.Message.Respond(s);
            }

            else
                await e.Message.Respond($"You need to specify your {(g == Gender.Female ? "daughteru" : "sonfu")}.");

        }

        private async Task Spouse(CommandEventArgs e) {

            string[] split = splitCommand(e.Message.Content, 2);
            string spouse = "";
            string sother = "";
            Gender g = Gender.Unspecified;

            if (split.Length == 1) {
                spouse = await data.GetSpouseName(e.Author.ID);
                sother = e.Author.Username;
                g = await data.GetSpouseGender(e.Author.ID);
            }
            else {
                spouse = await data.GetSpouseName(split[split.Length - 1]);
                sother = split[split.Length - 1];
                g = await data.GetSpouseGender(split[split.Length - 1]);
            }

            await e.Message.Respond(String.Format($"{sother}'s {(g == Gender.Female ? "waifu" : "husbando")} is {spouse}"));

        }

        private string[] splitCommand(string s, int segments) {

            return s.Split(new char[] { ' ' }, segments);

        }
        #endregion

    }

}
