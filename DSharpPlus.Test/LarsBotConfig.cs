﻿using System;
using Newtonsoft.Json;

namespace DSharpPlus.Bot
{
    internal sealed class LarsBotConfig
    {
        [JsonProperty("token")]
        public string Token { get; private set; }

        [JsonProperty("command_prefix")]
        public string CommandPrefix { get; private set; }

        public static LarsBotConfig Default { get { return _default.Value; } }
        private static Lazy<LarsBotConfig> _default = new Lazy<LarsBotConfig>(() => new LarsBotConfig { Token = string.Empty, CommandPrefix = "!" });

        private LarsBotConfig() { }
    }
}
